Pentesting met Python
---------------------

Voorbereiding
-------------

Zorg eerst dat de package manager `pip` geinstalleerd is.

OSX:
$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
$ python get-pip.py

Linux:
$ apt-get install python-pip

Windows:
Recente versies van Python hebben pip al geinstalleerd.

Maak een nieuw virtual environment genaamd `venv` aan en activeer het.
$ python3 -m venv venv
$ source venv/bin/activate

Installeer nu de packages vanuit requirements.txt.
$ pip install -r requirements.txt

Tenslotte kan je Jupyter Lab opstarten.
$ jupyter lab

Het is misschien nodig om Wireshark te installeren zodat je rechten goed staan om packets te filteren.


OSX Airport WIFI
----------------

OSX heeft een command line tool genaamd `airport` om WIFI mee te testen. Om het te gebruiken maak je eerst een symlink:
$ sudo ln -s /System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport /usr/local/bin/airport

Vervolgens geef je jezelf permissies om packets te filteren:
$ sudo chmod 0644 /dev/bpf*

Airport commando's:

Lijst van access points:
$ airport en0 -s

Sniffen:
$ airport en0 sniff

Zet WIFI kanaal handmatig tijdens sniffen:
$ airport en0 -c <kanaal>
