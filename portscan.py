#!/usr/bin/env python3
import sys
from enum import Enum
from scapy.all import IP, ICMP, TCP, sr1, RandShort


class ScanState(Enum):
    INVALID = 'invalid'
    CLOSED = 'closed'
    OPEN = 'open'
    FILTERED = 'filtered'

def tcp_stealth_scan(dst, dport, iface):
    sport = RandShort()
    # send syn
    syn = IP(dst=dst) / TCP(sport=sport, dport=dport, flags='S')
    ans = sr1(syn, verbose=0, timeout=1, iface=iface)
    if not ans:
        return ScanState.FILTERED
    elif ans.haslayer(TCP):
        # received syn+ack
        if ans[TCP].flags == 'SA':
            # respond with rst
            rst = IP(dst=dst) / TCP(sport=sport, dport=dport, flags='R')
            send(rst, verbose=0, iface=iface)
            return ScanState.OPEN
        # rst or rst+ack received, port is closed
        elif ans[TCP].flags & 0x4:
            return ScanState.CLOSED
    elif ans.haslayer(ICMP):
        if ans[ICMP].type == 3 and ans[ICMP].flags in (1, 2, 3, 9, 10, 13):
            return ScanState.FILTERED
    return ScanState.INVALID

def print_state(dport, state, proto):
    print('{:<10}{:<10}'.format('{}/{}'.format(dport, proto), state.value))

def scan(dst, dports, iface):
    print('{:<10}{:<10}'.format('PORT', 'STATE'))
    for dport in dports:
        state = tcp_stealth_scan(dst, dport, iface)
        if state != ScanState.CLOSED:
            print_state(dport, state, 'tcp')

if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument('dst')
    parser.add_argument('--min', default=1, type=int)
    parser.add_argument('--max', default=65535, type=int)
    parser.add_argument('--iface', default=None)
    args = parser.parse_args(sys.argv[1:])
    scan(args.dst, dports=range(args.min, 1+args.max), iface=args.iface)
