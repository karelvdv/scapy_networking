#!/usr/bin/env python3
import os
from scapy.all import Dot11Beacon, Dot11Elt, Dot11EltRSN, RadioTap, sniff
from manuf import parse_manuf

def setchannel(channel, iface):
    return os.system(f'iw dev {iface} set channel {channel}') == 0

def network_stats(pkt):
    summary = {}
    crypto = set()
    p = pkt.payload
    while isinstance(p, Dot11Elt):
        if p.ID == 0:
            summary['ssid'] = p.info.decode('utf-8')
        elif p.ID == 3:
            summary['channel'] = ord(p.info)
        elif isinstance(p, Dot11EltRSN):
            crypto.add('WPA2')
        elif p.ID == 221:
            crypto.add('WPA')
        p = p.payload
    if not crypto:
        if pkt.cap.privacy:
            crypto.add('WEP')
        else:
            crypto.add('OPN')
    summary['crypto'] = crypto
    return summary

def get_rssi(radiotap):
    if 'dBm_AntSignal' in radiotap.present:
        return radiotap.dBm_AntSignal
    return float('-inf')

def apscan(timeout, iface):
    access_points = []
    bssids = set()

    manufs = dict(parse_manuf())

    def prn_dot11(pkt):
        bssid = pkt.addr3.upper()
        if bssid not in bssids:
            stats = network_stats(pkt[Dot11Beacon])
            ssid = stats['ssid']
            chan = stats['channel']
            crypto = '/'.join(sorted(stats['crypto']))
            manuf = manufs.get(bssid[:8], '')
            rssi = get_rssi(pkt[RadioTap])
            access_points.append((bssid, ssid, chan, crypto, manuf, rssi))
            bssids.add(bssid)

    for chan in range(1, 14):
        print(f'{chan:02}..', end='', flush=True)
        setchannel(chan, iface)
        sniff(lfilter=lambda p: p.haslayer(Dot11Beacon), prn=prn_dot11, iface=iface, timeout=timeout)
    print()
    for bssid, ssid, chan, crypto, manuf, rssi in access_points:
        print(f'{chan:02} {rssi:4} {bssid} {manuf:8} {crypto:8} {ssid}')

if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument('--iface', help='Network interface')
    parser.add_argument('--timeout', help='Time to sniff on each channel', default=5, type=int)
    args = parser.parse_args(sys.argv[1:])
    sys.exit(apscan(timeout=args.timeout, iface=args.iface))
