#!/usr/bin/env python3
from scapy.all import IP, ICMP, sr1, RandShort

def traceroute(dst, iface, maxttl=64):
    for ttl in range(1, 1 + maxttl):
        ping = IP(dst=dst, ttl=ttl) / ICMP(id=RandShort())
        pong = sr1(ping, verbose=0, timeout=3, iface=iface)
        if pong:
            print(f'{ttl:04d} {pong[IP].src}')
            if pong[ICMP].type == 0:
                print('echo-reply received')
                break
        else:
            print(f'{ttl:04d} timeout')

if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser()
    parser.add_argument('hostname')
    parser.add_argument('--iface', default=None)
    parser.add_argument('--maxttl', default=64, type=int)
    args = parser.parse_args(sys.argv[1:])
    traceroute(args.hostname, iface=args.iface, maxttl=args.maxttl)
