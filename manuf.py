#!/usr/bin/env python3
import csv

def parse_manuf(filename='manuf.txt'):
    """
    Parses the Wireshark OUI table downloaded from
    https://code.wireshark.org/review/gitweb?p=wireshark.git;a=blob_plain;f=manuf
    """
    with open(filename) as f:
        c = csv.reader(f, delimiter='\t')
        for line in c:
            if len(line) >= 2 and len(line[0]) == 8:
                yield tuple(line[:2])
